CLI_CONFIG = {
    # The dyne will always be heist.
    # List the subcommands that will expose this option
    "key": {"subcommands": ["salt.minion"], "dyne": "heist"}
}
CONFIG = {
    # This will show up in hub.OPT.heist.key
    "key": {"help": "", "default": None}
}
SUBCOMMANDS = {"salt.minion": {"help": "", "dyne": "heist"}}
DYNE = {
    "artifact": ["artifact"],
    "heist": ["heist"],
    "salt": ["salt"],
    "service": ["service"],
}
